# ============================================================
# JavaScript Button extension for Falkon
# Copyright (C) 2019 Zdravko Mitov <mitovz@mail.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ============================================================
import os
import Falkon
from PySide2 import QtGui, QtWidgets
from PySide2.QtWebEngineWidgets import QWebEngineSettings


class JSButton(Falkon.AbstractButtonInterface):
    setts = {}
    view = None

    def __init__(self, window):
        super().__init__()
        self.window = window
        self.setTitle("JavaScript Button")
        self.setToolTip("Temporarily enable/disable JavaScript per-window")

        fb_icon = QtGui.QIcon(os.path.join(os.path.dirname(__file__), "js-button.svg"))
        self.setIcon(QtGui.QIcon.fromTheme("application-javascript", fb_icon))

        self.clicked.connect(self.on_toggle_javascript)
        self.webViewChanged.connect(self.on_webview_changed)

    def id(self):
        return "javascript-button"

    def name(self):
        return "JavaScript button"

    def current_page(self):
        if not self.window.weView():
            return
        return self.window.weView().page()

    def on_check_scheme(self):
        page = self.current_page()
        if page:
            return page.url().scheme()
        return

    def current_page_settings(self):
        if not self.window.weView():
            return
        return self.window.weView().page().settings()

    def test_current_page_attribute(self, attr):
        return self.current_page_settings() and self.current_page_settings().testAttribute(attr)

    def on_update_state(self):
        sheme = self.on_check_scheme()
        if not sheme or sheme == "falkon":
            self.setVisible(False)
            return
        self.setVisible(True)
        enabled = self.test_current_page_attribute(QWebEngineSettings.JavascriptEnabled)
        self.setActive(enabled)
        if enabled:
            self.setToolTip("Disable JavaScript")
        else:
            self.setToolTip("Enable JavaScript")

    def set_current_page_attribute(self, attr, value):
        if self.current_page_settings():
            self.current_page_settings().setAttribute(attr, value)

    def on_set_attribute_cb(self, page, isMainFrame):
        if isMainFrame:
            page.settings().setAttribute(QWebEngineSettings.JavascriptEnabled, self.setts[page])

    def on_toggle_javascript(self):
        page = self.current_page()
        if not page:
            return
        elif self.on_check_scheme() == "falkon":
            QtWidgets.QMessageBox.warning(page.view(), "Not Permited",
                                          "JavaScript cannot be disabled on this page!")
            return
        current = self.test_current_page_attribute(QWebEngineSettings.JavascriptEnabled)
        self.set_current_page_attribute(QWebEngineSettings.JavascriptEnabled, not current)
        self.setts[page] = not current
        page.navigationRequestAccepted.connect(lambda u, t, f: self.on_set_attribute_cb(page, f))
        self.window.weView().reload()

    def on_webview_changed(self, view):
        self.on_update_state()
        if self.view:
            self.view.loadFinished.disconnect(self.on_update_state)
        self.view = view
        if self.view:
            self.view.loadFinished.connect(self.on_update_state)
