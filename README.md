### A tool-bar java-script button for [Falkon](https://phabricator.kde.org/source/falkon/) web browser.

### Note:
The files listed here are not official part of **Falkon**, so you should not refer any related issue there!
